import 'package:flutter/material.dart';

class IdentityCard extends StatefulWidget {
  @override
  _IdentityCardState createState() => _IdentityCardState();
}

class _IdentityCardState extends State<IdentityCard> {
  @override
  int yearsOfExperience = 0;

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[900],
      appBar: AppBar(
        backgroundColor: Colors.grey[850],
        elevation: 0.0,
        title: Text(
          'Employee Profile',
          style: TextStyle(
            color: Colors.blue,
            fontSize: 30,
          ),
        ),
        centerTitle: true,
      ),
      body: Padding(
        padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 0.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Center(
              child: CircleAvatar(
                backgroundImage: AssetImage('images/avatarFlo.png'),
                radius: 80.0,
              ),
            ),
            Divider(
              height: 50.0,
              color: Colors.grey[800],
            ),
            Text(
              'NAME: ',
              style: TextStyle(
                color: Colors.white,
                letterSpacing: 2.0,
              ),
            ),
            SizedBox(height: 10.0),
            Text(
              'Florentin Lupascu',
              style: TextStyle(
                color: Colors.amberAccent,
                letterSpacing: 2.0,
                fontSize: 28.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 30.0),
            Text(
              'ROLE TITLE: ',
              style: TextStyle(
                color: Colors.white,
                letterSpacing: 2.0,
              ),
            ),
            SizedBox(height: 10.0),
            Text(
              'Flutter Developer',
              style: TextStyle(
                color: Colors.amberAccent,
                letterSpacing: 2.0,
                fontSize: 28.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 30.0),
            Text(
              'YEARS IN COMPANY: ',
              style: TextStyle(
                color: Colors.white,
                letterSpacing: 2.0,
              ),
            ),
            SizedBox(height: 10.0),
            Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    '$yearsOfExperience',
                    style: TextStyle(
                      color: Colors.amberAccent,
                      letterSpacing: 2.0,
                      fontSize: 28.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  flex: 4,
                ),
                Expanded(
                  child: FloatingActionButton(
                    onPressed: () {
                      setState(() {
                        yearsOfExperience += 1;
                      });
                    },
                    child: Icon(
                      Icons.add,
                      size: 20,
                      color: Colors.amberAccent,
                    ),
                    backgroundColor: Colors.grey[700],
                  ),
                  flex: 1,
                ),
                Expanded(
                  child: FloatingActionButton(
                    onPressed: () {
                      setState(() {
                        if (yearsOfExperience <= 0) {
                          yearsOfExperience = 0;
                        } else {
                          yearsOfExperience -= 1;
                        }
                      });
                    },
                    child: Icon(
                      Icons.remove,
                      size: 20,
                      color: Colors.amberAccent,
                    ),
                    backgroundColor: Colors.grey[700],
                  ),
                  flex: 1,
                ),
              ],
            ),
            SizedBox(height: 20.0),
            Row(
              children: <Widget>[
                Icon(
                  Icons.email,
                  color: Colors.grey[400],
                ),
                SizedBox(width: 10.0),
                Text(
                  'flo@yahoo.com',
                  style: TextStyle(
                    color: Colors.grey[300],
                    fontSize: 18.0,
                    letterSpacing: 1.0,
                  ),
                ),
              ],
            ),
            Expanded(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: RaisedButton.icon(
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (_) => AlertDialog(
                        title: Text(
                          'Call options',
                          style: TextStyle(
                            color: Colors.blueAccent,
                            fontSize: 24,
                          ),
                        ),
                        content: Text(
                          'Are you sure you want to make this call ?',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                          ),
                        ),
                        actions: <Widget>[
                          new FlatButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text(
                              'No',
                              style: TextStyle(
                                color: Colors.blueAccent,
                                fontSize: 20,
                              ),
                            ),
                          ),
                          new FlatButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text(
                              'Yes',
                              style: TextStyle(
                                color: Colors.blueAccent,
                                fontSize: 20,
                              ),
                            ),
                          ),
                        ],
                        elevation: 10.0,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(20.0))),
                        backgroundColor: Colors.grey[900],
                      ),
                      barrierDismissible: false,
                    );
                  },
                  icon: Icon(
                    Icons.phone_iphone,
                    size: 40,
                  ),
                  label: Text(
                    'Contact me',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),
                  ),
                  color: Colors.amber,
                  elevation: 5,
                ),
              ),
            ),
            SizedBox(height: 10.0)
          ],
        ),
      ),
    );
  }
}
