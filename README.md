# IdentityCard

**Version 1.0.0**

**Flutter 1.20**

This project was created in Flutter using Dart and represent a profile of an employee from a company. On that profile you can see the photo of that person, name, role, email address, years in company and also you can contact the person by clicking on a button and a pop-up will show up. I developed this small app to improve my skills on Google Flutter and to play around with some widgets.

##                                                            VIEW SCREENSHOTS !

![Screenshot 1](demo/IdentityCard_Photo_1.png)
![Screenshot 2](demo/IdentityCard_Photo_2.png)

## Contributors
@ Florentin Lupascu <lupascu_florentin@yahoo.com>

## License & Copyright
© Florentin Lupascu, IdentityCard

Licensed under the [MIT License](LICENSE).
